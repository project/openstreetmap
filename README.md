# OpenStreetMap

This module enables creating and syncing drupal nodes (nodes) based on
OpenStreetMap nodes and ways (OSM nodes). By default, it stores the ID and the
name of the OSM node, but any fields added to the Drupal node type present on
the synced entity will also be pulled in.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/openstreetmap).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/openstreetmap).


## Table of contents

- Requirements
- Recommended modules
- Installation
- Configuration
- Manually adding nodes
- Configuring overpass queries
- FAQ
- Maintainers


## Requirements

This module requires the following modules:

- [Geofield](https://www.drupal.org/project/geofield)
- Overpass interpreter URL
  It ships with a set of utilities for applying overpass queries and importing
  the result of those queries as OSM nodes. You will need to enter an Overpass
  interpreter URL `https://wiki.openstreetmap.org/wiki/Overpass_API#Public_Overpass_API_instances`


## Recommended modules

To place these on a map, use any Drupal module capable of mapping data in the
Geodata field. I use the Leaflet Views submodule of
[Leaflet](https://www.drupal.org/project/leaflet) to great effect.

- [Leaflet](https://www.drupal.org/project/leaflet)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration


1. Configure the OSM Settings
   Navigate to Administration > Config > OSM Settings (/admin/config/osm)

  - You will need to enter an Overpass interpreter URL.
    `https://wiki.openstreetmap.org/wiki/Overpass_API#Public_Overpass_API_instances`

2. Create a new OSM Node type
   Navigate to Administration > Structure > OSM Node Type

  - Add OSM Node type
    Create a new OSM Node type, like `"Business"`

  - Add fields as desired, such as `"wheelchair"`
    Anything that matches an OSM tag (with or without the `field_` prefix).
    For instance, if you have a field named `"field_bus"` and the OSM Node
    has a tag `"bus: yes"`, then your Drupal node's field will have `"yes"`


## Manually adding nodes

If you only have a handful of nodes that you want to keep in sync, you can
add them individually using their OSM IDs obtained at [openstreetmap](https://openstreetmap.org)

- Navigate to Administration > OSM Node List

- Add OSM Node
  Fill out the OSM node ID.
  You can get the OSM node ID by using the online OSM editor.
  You can also use ways, but check the `"Is way"` box.

- Save OSM Node
  On save, the OpenStreetMap module will get all matching tags as field values.


## Configuring overpass queries

The Overpass language [Language_Guide](https://wiki.openstreetmap.org/wiki/Overpass_API/Language_Guide)
can be used for querying features in OpenStreetMap. This is the easiest
way to keep a group of nodes in sync with the OpenStreetMap module.
It requires the "OpenStreetMap Query Tools" submodule to be installed.
Use `https://overpass-turbo.eu/` to test queries

- Structure > Overpass Queries > Add Query (`/admin/structure/osm_query/add`)

- Give the query a title (`"All Bus Stops in Atlanta"`)

- Fill out the query body using Overpass Syntax

- Select the bundle (OSM Node Type) that you want nodes matching this
  query to be saved into.
- Save the query.

- Go to the `"Execute"` tab of the newly created query. There you can preview
  how many nodes the query will enter into the system.

- You can execute the query from this page, starting a batch operation or

- Visiting Administration > Content > OSM Node List, then clicking
  `"Sync All"` (`/admin/content/osm_node/sync`)

NOTE: You can also run a query once by visiting
Administration > Content > Import OSM Nodes (`admin/content/osm_node/import`)


## Faq


To place these on a map, use any Drupal module capable of mapping data in the
Geodata field. I use [Leaflet](https://www.drupal.org/project/leaflet) to great effect.

- Enable the Leaflet and the Leaflet Views module
- Create a new view of OSM nodes
- Add the Geodata field
- Set up the Leaflet view type settings to get geodata from the Geodata field
- The map will now display all OSM nodes matching that query


## Maintainers

- Jason Salter - [jpsalter](https://www.drupal.org/u/jpsalter)
- Tyler Cifu Shuster - [tbcs](https://www.drupal.org/u/tbcs)
