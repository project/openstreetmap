<?php

namespace Drupal\openstreetmap\Field\Computed;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

class OSMEditLink extends FieldItemList {
  use ComputedItemListTrait;

  protected function computeValue() {
    /** @var \Drupal\openstreetmap\Entity\OSMNode $osm_node */
    $osm_node = $this->getEntity();

    $this->list[0] = $this->createItem(
      0,
      $osm_node->toUrl('osm.edit')->toString()
    );
  }

}
