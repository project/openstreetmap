<?php

namespace Drupal\openstreetmap\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for OSM Node entities.
 */
class OSMNodeViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $views_data = parent::getViewsData();
    $views_data['osm_node_field_data']['osm_edit_link'] = [
      'title' => $this->t('OSM Edit Link'),
      'field' => [
        'id' => 'field',
        'default_formatter' => 'string',
        'field_name' => 'osm_edit_link'
      ]
    ];
    $views_data['osm_node_field_data']['osm_view_link'] = [
      'title' => $this->t('OSM View Link'),
      'field' => [
        'id' => 'field',
        'default_formatter' => 'string',
        'field_name' => 'osm_view_link'
      ]
    ];
    return $views_data;
  }

}
